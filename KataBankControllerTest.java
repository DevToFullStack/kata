package Test.Bank;
/*
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
/*
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
public class KataBankControllerTest {

   @Autowired
   private WebApplicationContext webApplicationContext;
   private MockMvc mockMvc;

   @Mock
   private BankService bankService;

   private Account account;

   private String URI = "api/account";

   private String inputJson;

   @Before
   public void mockBeans() throws JsonProcessingException {
      this.account = Account.getInstance("Damien", "Damien289");
      this.inputJson = this.mapToJson(account);
      mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
   }

   @Test
   public void testCheckAccountBalance() throws Exception {

      RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get(URI + "/balance")
            .accept(MediaType.APPLICATION_JSON)
            .content(inputJson)
            .contentType(MediaType.APPLICATION_JSON);
      when(bankService.checkAccountBalance(Mockito.any(Account.class))).thenReturn(account);
      MvcResult result = mockMvc.perform(requestBuilder).andReturn();

      MockHttpServletResponse response = result.getResponse();

      String outPutJson = response.getContentAsString();

      assertEquals(outPutJson, account);
   }

   @Test
   public void testDeposit() throws Exception {
      account.setBalance(account.getAmount() + 3000);
      RequestBuilder requestBuilder = MockMvcRequestBuilders
            .put(URI + "/deposit")
            .accept(MediaType.APPLICATION_JSON)
            .content(inputJson)
            .contentType(MediaType.APPLICATION_JSON);
      when(bankService.deposit(Mockito.any(Account.class), Mockito.any())).thenReturn(account);
      MvcResult result = mockMvc.perform(requestBuilder).andReturn();

      MockHttpServletResponse response = result.getResponse();

      String outPutJson = response.getContentAsString();

      assertEquals(outPutJson, account);
   }

   @Test
   public void testWithDrawn() throws Exception {
      account.setBalance(account.getAmount() - 2000);
      RequestBuilder requestBuilder = MockMvcRequestBuilders
            .put(URI + "/withdraw")
            .accept(MediaType.APPLICATION_JSON)
            .content(inputJson)
            .contentType(MediaType.APPLICATION_JSON);
      when(bankService.withdraw(Mockito.any(Account.class), Mockito.any())).thenReturn(account);
      MvcResult result = mockMvc.perform(requestBuilder).andReturn();

      MockHttpServletResponse response = result.getResponse();

      String outPutJson = response.getContentAsString();

      assertEquals(outPutJson, account);
   }

   @Test
   public void testGetHistories() throws Exception {
      account.setBalance(account.getAmount() - 2000);
      RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get(URI + "/histories")
            .accept(MediaType.APPLICATION_JSON)
            .content(inputJson)
            .contentType(MediaType.APPLICATION_JSON);
      when(bankService.getHistories(Mockito.any(Account.class))).thenReturn(account);
      MvcResult result = mockMvc.perform(requestBuilder).andReturn();

      MockHttpServletResponse response = result.getResponse();

      String outPutJson = response.getContentAsString();

      assertEquals(outPutJson, account);
   }

   @Test
   public void testGetAmount() throws Exception {
      RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get(URI + "/operationAmount")
            .accept(MediaType.APPLICATION_JSON)
            .content(inputJson)
            .contentType(MediaType.APPLICATION_JSON);
      when(bankService.getAmount(Mockito.any(Account.class))).thenReturn(account.getAmount());
      MvcResult result = mockMvc.perform(requestBuilder).andReturn();

      MockHttpServletResponse response = result.getResponse();

      String outPutJson = response.getContentAsString();

      assertEquals(outPutJson, account.getAmount());
   }

   @Test
   public void testCalculateInterest() throws Exception {

      RequestBuilder requestBuilder = MockMvcRequestBuilders
            .get(URI + "/interest")
            .accept(MediaType.APPLICATION_JSON)
            .content(inputJson)
            .contentType(MediaType.APPLICATION_JSON);
      when(bankService.calculateInterest(Mockito.any(Account.class), Mockito.any()))
            .thenReturn(Double.valueOf(35000));
      MvcResult result = mockMvc.perform(requestBuilder).andReturn();

      MockHttpServletResponse response = result.getResponse();

      String outPutJson = response.getContentAsString();

      assertEquals(outPutJson, "35000");
   }

   /**
    * Maps an object into a JSON String using a Jackson ObjectMapper
    *
    * @param object to mapping as Json string
    *
    * @return object in a Json string
    *
    * @throws JsonProcessingException
    */
  /* private String mapToJson(Object object) throws JsonProcessingException {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(object);
   }
}*/
