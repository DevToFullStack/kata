package Test.Bank;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
//import javax.transaction.Transactional;
//import org.springframework.stereotype.Service;

//@Service
/*@Transactional
public class DefaultBankService implements BankService {

   @Override
   public Account checkAccountBalance(Account account) {
      System.out.println("**************************************************");
      System.out.println("Balance = " + account.getBalance() + "€");
      System.out.println("**************************************************");
      System.out.println();
      return account;
   }

   @Override
   public Account deposit(Account account, int amountToDeposit) {
      if (amountToDeposit > 0) {
         account.setBalance(account.getBalance() + amountToDeposit);
         account.setAmount(amountToDeposit);
         account.setLocalDate(LocalDate.now());
         List<AccountStatement> histories = account.getAccountStatements();
         histories.add(new AccountStatement("Deposit",
                                            account.getLocalDate(),
                                            account.getAmount(),
                                            account.getBalance()));
         account.setAccountStatements(histories);
      }
 return account;
   }

   @Override
   public Account withdraw(Account account, int amountToWithdraw) {
      if (amountToWithdraw > 0 && amountToWithdraw <= account.getBalance()) {
         account.setBalance(account.getBalance() - amountToWithdraw);
         account.setAmount(-amountToWithdraw);
         account.setLocalDate(LocalDate.now());
         List<AccountStatement> histories = account.getAccountStatements();
         histories.add(new AccountStatement("WithDraw",
                                            account.getLocalDate(),
                                            account.getAmount(),
                                            account.getBalance()));
         account.setAccountStatements(histories);
      } else {
         System.out.println("the balance is insufficient");
      }
 return account;
   }

   @Override
   public Account getHistories(Account account) {
      List<AccountStatement> histories = account.getAccountStatements();
      System.out.println("");
      if (!histories.isEmpty())
         System.out.println("Operation type     | operation Date      | amount       | Balance");
      account.getAccountStatements().stream().map(history -> {
         String accountHistory = history.getTypeOperation()
                     + "              "
                     + history.getLocalDate()
                     + "            "
                     + history.getAmount()
                     + "           "
                     + history.getBalance();
         System.out.println(accountHistory);
         System.out.println();
         return accountHistory;
      }).collect(Collectors.toList());
      if (histories.isEmpty()) System.out.println("There is no history of operations");
      System.out.println("*************************************************");
      System.out.println();
      return account;
   }

   @Override
   public int getAmount(Account account) {
      System.out.println("*************************************************");
      if (account.getAmount() > 0) {
         System.out.println("Deposited : " + account.getAmount());
         return account.getAmount();
      } else if (account.getAmount() < 0) {
         System.out.println("withdrawn : " + account.getAmount());
         return account.getAmount();
      } else {
         System.out.println("No transaction occurred : ");
      }
      System.out.println("*************************************************");
      System.out.println();
      return 0;
   }

   @Override
   public double calculateInterest(Account account, int years) {
      double interestRate = 0.75;
      double newBalance = (account.getBalance() * interestRate * years) + account.getBalance();
      System.out.println("The current interest rate is " + 100 * interestRate + "%");
      System.out.println("After " + years + " years, your balance will be : " + newBalance);
      System.out.println();
      return newBalance;
   }
}*/
