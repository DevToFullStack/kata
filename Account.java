package Test.Bank;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * This class Account allows to perform different operations related to the bank account
 */
public class Account {

   private static Account SINGLE_INSTANCE = null;
   // instance variables
   private int balance;
   private int amount;
   private LocalDate localDate;
   private String customerName;
   private String customerId;
   private List<AccountStatement> accountStatements;

   private Account(String customerName, String customerId) {
      this.customerName = customerName;
      this.customerId = customerId;
      this.accountStatements = new ArrayList<>();
   }

   public static Account getInstance(String customerName, String customerId) {
      if (SINGLE_INSTANCE == null) SINGLE_INSTANCE = new Account(customerName, customerId);

      return SINGLE_INSTANCE;
   }

   public String getCustomerName() {
      return customerName;
   }

   public String getCustomerId() {
      return customerId;
   }

   public int getBalance() {
      return balance;
   }

   public void setBalance(int balance) {
      this.balance = balance;
   }

   public int getAmount() {
      return amount;
   }

   public void setAmount(int amount) {
      this.amount = amount;
   }

   public LocalDate getLocalDate() {
      return localDate;
   }

   public void setLocalDate(LocalDate localDate) {
      this.localDate = localDate;
   }

   public void setAccountStatements(List<AccountStatement> accountStatements) {
      this.accountStatements = accountStatements;
   }

   public List<AccountStatement> getAccountStatements() {
      return accountStatements;
   }
}
