package Test.Bank;
/*
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
/*
public class KataBankServicesTest {

   @Mock
   private BankService bankService;

   @Mock
   private Account account;

   @Before
   public void mockBeans() {
      this.bankService = new DefaultBankService();
      this.account = Account.getInstance("Damien", "Damien289");
   }

   @After
   public void cleanMockBeans() {
      this.account = Account.getInstance("Damien", "Damien289");
   }

   @Test
   public void testAccount() {
      assertEquals("Damien", account.getCustomerName());
      assertEquals("Damien289", account.getCustomerId());
      assertEquals(0, account.getBalance());
      assertEquals(0, account.getAmount());
      assertEquals(null, account.getLocalDate());
      assertEquals(0, account.getAccountStatements().size());
   }

   @Test
   public void testDeposit() {
      bankService.deposit(account, 3000);
      bankService.deposit(account, 1500);
      bankService.deposit(account, 2500);
      assertEquals(7000, account.getBalance());
      assertEquals(LocalDate.now(), account.getLocalDate());
      assertEquals(3, account.getAccountStatements().size());
   }

   @Test
   public void testWithDrawn() {
      bankService.deposit(account, 10000);
      bankService.withdraw(account, 3000);
      assertEquals(14000, account.getBalance());
      assertEquals(-3000, account.getAmount());
      assertEquals(LocalDate.now(), account.getLocalDate());
      assertEquals(5, account.getAccountStatements().size());
   }

   @Test
   public void testCheckAccountBalance() {
      bankService.deposit(account, 20000);
      bankService.withdraw(account, 3000);
      assertEquals(106000, account.getBalance());
      bankService.deposit(account, 600);
      assertEquals(106600, account.getBalance());
   }

   @Test
   public void testCalculateInterest() {
      bankService.deposit(account, 50000);
      bankService.withdraw(account, 15000);
      bankService.deposit(account, 40000);
      assertEquals(222500.0, bankService.calculateInterest(account, 2), 0);
   }

}*/
