package Test.Bank;

public interface BankService {

   Account checkAccountBalance(Account account);

   /**
    * The method is used to deposit money into the account
    *
    * @param account customer's account
    * @param amountToDeposit amount to deposit
    * @return account of customer
    */
   Account deposit(Account account, int amountToDeposit);

   /**
    * The method is used to withdraw money from the account
    *
    * @param account account customer's account
    * @param amountToWithdraw amountToWithdraw amount to withdraw
    * @return account of customer
    */
   Account withdraw(Account account, int amountToWithdraw);

   /**
    * Obtain the history of operations
    *
    * @param account account customer's account
    * @return account of customer
    */
   Account getHistories(Account account);

   /**
    * The method is used to obtain the previous transaction
    *
    * @param account customer's account
    * @return amount of previous transaction
    */
   int getAmount(Account account);

   /**
    * The method serves the interests of the account
    *
    * @param account customer's account
    * @param years interest's years
    * @return interest of customer's account for number of years
    */
   double calculateInterest(Account account, int years);
}
