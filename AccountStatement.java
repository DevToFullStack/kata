package Test.Bank;

import java.time.LocalDate;

/**
 * The historization class of an operation
 */
public class AccountStatement {

   private String typeOperation;
   private LocalDate localDate;
   private int amount;
   private int balance;

   public AccountStatement(String typeOperation, LocalDate localDate, int amount, int balance) {
      this.typeOperation = typeOperation;
      this.localDate = localDate;
      this.amount = amount;
      this.balance = balance;
   }

   public String getTypeOperation() {
      return typeOperation;
   }

   public void setTypeOperation(String typeOperation) {
      this.typeOperation = typeOperation;
   }

   public LocalDate getLocalDate() {
      return localDate;
   }

   public int getAmount() {
      return amount;
   }

   public int getBalance() {
      return balance;
   }
}
